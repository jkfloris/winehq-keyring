# winehq-keyring
Add the WineHQ repository with a Debian package
This allows to install Wine on Ubuntu/Debian with:

`wget https://some-url/winehq-keyring.deb`

`sudo dpkg -i winehq-keyring.deb`

`sudo apt update`

`sudo apt install winehq-stable`


## Description
This package does two things:
- Install winehq.key as /usr/share/keyrings/winehq-archive.key (winehq-keyring.install)
- Create the winehq.sources file in /etc/apt/sources.list.d/ (postinst)

Should postinst fail to create the winehq.sources file, only the key will be installed.
